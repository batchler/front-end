import React, { useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faAngleDown,
    faAngleUp,
    faTrash
} from "@fortawesome/free-solid-svg-icons";
import {Switch} from "@headlessui/react";
import {ValidationTab} from "@/components/validation/validationTab";



export function ExerciseTab({exercise}) {
    const [isOpen, setOpen] = useState(false);
    const [enabled, setEnabled] = useState(true);

    return (
        <div>
            <div >
                    <div className="rounded-lg bg-karl dark:bg-karl-dark  font-arial p-3 mt-4">
                        <div className="flex justify-between" onClick={() => setOpen(!isOpen)}>
                            <p className="mt-auto mb-auto">{isOpen && (
                                <FontAwesomeIcon className="opacity-50 mr-2" icon={faTrash}/>)} {exercise.name} <span
                                className="text-xs">{exercise.slug}</span></p>
                            <FontAwesomeIcon className=" items-center mt-auto mb-auto"
                                             icon={!isOpen ? faAngleDown : faAngleUp}/>
                        </div>
                        {isOpen && (
                            <div className="ml-7 ">
                                <div className="flex mt-3 mb-6">
                                    <div className="flex flex-col">
                                        <label htmlFor="startDate">Start Date:</label>
                                        <input className="input text-lg" id="startDate" type="datetime-local"/>
                                    </div>
                                    <div className="flex flex-col">
                                        <label htmlFor="endDate">End Date:</label>
                                        <input className="input text-lg" id="endDate" type="datetime-local"/>
                                    </div>
                                    <div className="flex flex-col ml-2">
                                        <p>Online: </p>
                                        <Switch
                                            checked={enabled}
                                            onChange={setEnabled}
                                            className={`${enabled ? 'bg-blue-600' : 'bg-blue-300'}
          relative inline-flex h-[21px] w-[37px] shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus-visible:ring-2 mt-auto mb-auto self-center  focus-visible:ring-white focus-visible:ring-opacity-75`}
                                        >
                                            <span className="sr-only">put online</span>
                                            <span
                                                aria-hidden="true"
                                                className={`${enabled ? 'translate-x-4' : 'translate-x-0'}
            pointer-events-none inline-block h-[17px] w-[17px] transform rounded-full bg-white shadow-lg ring-0 transition duration-200 ease-in-out`}
                                            />
                                        </Switch></div>

                                </div>


                                <h2 className="text-2xl font-arial ">Students:</h2>
                                <div className="border-2 border-gray-300 rounded-lg mt-2">
                                    {
                                        exercise.validations.map((validation)=>(
                                            <ValidationTab validationData={validation}/>
                                        ))
                                    }

                                </div>



                            </div>
                        )}
                    </div>

            </div>
        </div>

    );
}


