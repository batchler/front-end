import React, {Fragment, useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faAngleDown,
    faAngleUp,
    faMobileScreenButton,
    faTabletScreenButton,
    faDisplay
} from "@fortawesome/free-solid-svg-icons";
import {Tab} from "@headlessui/react";
import axios from "axios";


export function ValidationTab({zipFile, validationData=null}) {
    const [isOpen, setOpen] = useState(false);
    const [isUploading, setUploading] = useState(true)
    const [validation, setValidation] = useState(true)

    useEffect(() => {
        if (validation !== null) {
            setValidation(validationData);
            console.log(validationData)
            setUploading(false);
        } else {
            uploadZip();
        }

    }, []);

    const uploadZip = async () => {
        const formData = new FormData();
        formData.append('file', zipFile);

        const res = await axios.post('http://localhost:8888/api/validate', formData);
        const jsonData = res.data;
        if (res.status === 200) {
            setUploading(false);
            setValidation(jsonData);
            console.log(jsonData)
        }


    }


    return (
        <div>
            {isUploading ?
                <div className="rounded-lg bg-karl dark:bg-karl-dark flex justify-between font-arial p-3 mt-1.5">
                    <p>{zipFile? zipFile.name:``}</p>
                    <p>Uploading...</p>
                </div> :
                <div className="rounded-lg bg-karl dark:bg-karl-dark  font-arial p-3 mt-1.5">
                    <div className="flex justify-between" onClick={() => setOpen(!isOpen)}>
                        <p>{zipFile? zipFile.name:`${validation.student.firstname} ${validation.student.lastname}`}</p>
                        <p>{validation.score}/100
                            <FontAwesomeIcon className="ml-2" icon={!isOpen ? faAngleDown : faAngleUp}/>
                        </p>
                    </div>
                    {isOpen && (
                        <div>

                            <Tab.Group>
                                <Tab.List
                                    className="flex justify-between shadow-lg mt-4 p-1.5 rounded-lg text-primary-dark bg-white mb-2">
                                    <Tab className="flex" as={Fragment}>
                                        {({selected}) => (
                                            <button className={selected ? "tab-content-selected " : "tab-content"}>
                                                <FontAwesomeIcon className="mr-2" icon={faMobileScreenButton}/>360x640
                                            </button>
                                        )}</Tab>
                                    <Tab className="flex" as={Fragment}>
                                        {({selected}) => (
                                            <button className={selected ? "tab-content-selected " : "tab-content"}>
                                                <FontAwesomeIcon className="mr-2" icon={faTabletScreenButton}/>768x1024
                                            </button>
                                        )}</Tab>
                                    <Tab className="flex" selected={true} as={Fragment}>
                                        {({selected}) => (
                                            <button className={selected ? "tab-content-selected" : "tab-content"}>
                                                <FontAwesomeIcon className="mr-2" icon={faDisplay}/>
                                                1366x768
                                            </button>
                                        )}</Tab>
                                    <Tab selected={true} as={Fragment}>
                                        {({selected}) => (
                                            <button className={selected ? "tab-content-selected" : "tab-content"}>
                                                <FontAwesomeIcon className="mr-2" icon={faDisplay}/>
                                                1920x1080
                                            </button>
                                        )}</Tab>
                                </Tab.List>
                                <Tab.Panels>
                                    <Tab.Panel>
                                        <div className="flex justify-around max-h-screen overflow-auto">
                                            <img src={`http://localhost:8888/${validation.images[0].submittedImage}`}
                                                 alt="studentImage" className="max-w-1/2 h-min"/>
                                            <img src={`http://localhost:8888/${validation.images[0].solutionImage}`}
                                                 alt="studentImage" className="max-w-1/2 h-min"/>
                                        </div>

                                    </Tab.Panel>
                                    <Tab.Panel>
                                        <div className="flex justify-around max-h-screen overflow-auto">
                                            <img src={`http://localhost:8888/${validation.images[1].submittedImage}`}
                                                 alt="studentImage" className="max-w-1/2 h-min"/>
                                            <img src={`http://localhost:8888/${validation.images[1].solutionImage}`}
                                                 alt="studentImage" className="max-w-1/2 h-min"/>
                                        </div>
                                    </Tab.Panel>
                                    <Tab.Panel>
                                        <div className="flex justify-around max-h-screen overflow-auto">
                                            <img src={`http://localhost:8888/${validation.images[2].submittedImage}`}
                                                 alt="studentImage" className="max-w-1/2 h-min"/>
                                            <img src={`http://localhost:8888/${validation.images[2].solutionImage}`}
                                                 alt="studentImage" className="max-w-1/2 h-min"/>
                                        </div>
                                    </Tab.Panel>
                                </Tab.Panels>
                                <Tab.Panel>
                                    <div className="flex justify-around max-h-screen overflow-auto">
                                        <img src={`http://localhost:8888/${validation.images[3].submittedImage}`}
                                             alt="studentImage" className="max-w-1/2 h-min"/>
                                        <img src={`http://localhost:8888/${validation.images[3].solutionImage}`}
                                             alt="studentImage" className="max-w-1/2 h-min"/>
                                    </div>
                                </Tab.Panel>
                            </Tab.Group>

                            <h2 className="text-2xl font-arial mt-4">Warnings:</h2>
                            {validation.warnings.map((warning) => (
                                <div>
                                    <div
                                        className="bg-red-200 p-3 rounded-lg mt-5 text-primary-dark border border-red-500">
                                        <div className="flex justify-between">
                                            <p><span
                                                className="bg-red-600 rounded-lg text-white p-1">{warning.file.split('/').slice(-1)[0]}</span> {warning.message}</p>
                                            <p className="bg-red-600 rounded-full w-8 h-8 flex items-center justify-center text-white">{warning.penalty}
                                            </p>
                                        </div>
                                        <p className="ml-2">{warning.line}</p>
                                    </div>
                                </div>
                            ))}


                            <h2 className="text-2xl font-arial mt-4">Plagiarism:</h2>

                            <div className="relative overflow-x-auto mt-5">
                                <table className="w-full text-sm text-left text-gray-500 shadow-2xl">
                                    <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                                    <tr>
                                        <th scope="col" className="px-6 py-3">
                                            Student name
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Student validationID
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Student file
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Current student file
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Percentage %
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {validation.plagiarism[0] && validation.plagiarism[0].files.map((file) => (
                                        <tr className="bg-white border-b ">
                                            <th scope="row"
                                                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                                                {file.student.firstname} {file.student.lastname}
                                            </th>
                                            <th scope="row"
                                                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                                                {file.comparedID}
                                            </th>
                                            <td className="px-6 py-4">
                                                {file.comparedFilename}
                                            </td>
                                            <td className="px-6 py-4">
                                                {file.filename}
                                            </td>
                                            <td className="px-6 py-4">
                                                {file.percentage}
                                            </td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    )}
                </div>}
        </div>

    );
}


