import React, {useEffect, useState} from "react";
import {ExerciseTab} from "@/components/exercise/exerciseTab";
import axios from "axios";


export function PublishedTab(){

    const [exercisesDetailed, setExercisesDetailed] = useState([])

    useEffect(() => {
        getExercises()
    }, [])

    async function getExercises() {
        const res = await axios.get('http://localhost:8888/api/exercise/');
        const jsonData = res.data;
        for (let exercise of jsonData) {
            const resDetail = await axios.get(`http://localhost:8888/api/exercise/${exercise.id}`);
            setExercisesDetailed(prevExercises => [...prevExercises, resDetail.data]);}

    }

    return (
        <div>
            {exercisesDetailed.length!==0 && exercisesDetailed.map((exercise) =>
            <ExerciseTab exercise={exercise}/>
            )}
        </div>
    );
}


