import React, {useEffect, useState} from "react";
import {FileUploader} from "react-drag-drop-files";
import axios from 'axios';

export function CreateTab(){
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [exerciseName, setExerciseName] = useState('');
    const [exerciseSlug, setExerciseSlug] = useState('');
    const [exerciseReturn, setExerciseReturn] = useState()
    const [zipFile, setZipFile] = useState(null);
    const [requirements, setRequirements] = useState();
    const [reqs, setReqs] = useState(["display: flex"]);
    const [bans, setBans] = useState(["display: grid"]);
    const [currentReq, setCurrentReq] = useState('');
    const [currentBan, setCurrentBan] = useState('');

    useEffect(() => {
        const today = new Date();
        const defaultStartDate = today.toISOString().slice(0, 16);
        const sevenDaysLater = new Date(today);
        sevenDaysLater.setDate(today.getDate() + 7);
        sevenDaysLater.setHours(25, 59); // Set time to 23:59:59 in local timezone
        const defaultEndDate = sevenDaysLater.toISOString().slice(0, 16);

        setStartDate(defaultStartDate);
        setEndDate(defaultEndDate);
    }, []);

    const handleStartDateChange = (event) => {
        setStartDate(event.target.value);
    };

    const handleEndDateChange = (event) => {
        const newEndDate = event.target.value;
        if (newEndDate >= startDate) {
            setEndDate(newEndDate);
        }
    };

    const uploadZip = async () => {
        console.log("zip",zipFile)
        const formData = new FormData();
        console.log("ddddd")
        formData.append('file', zipFile);
        console.log(formData)

        const res = await axios.post('http://localhost:8888/api/exercise/upload', formData);
        const jsonData = res.data;
        setExerciseReturn(jsonData);
        setRequirements({})
    }
    const createExercise = async () => {
        const body = {
            name: exerciseName,
            slug: exerciseSlug,
            path: exerciseReturn.path,
            fonts: exerciseReturn.fonts,
            colors: exerciseReturn.colors,
            banned: bans,
            include: reqs
        }
        console.log(body)

        const res = await axios.post('http://localhost:8888/api/exercise', body,{
            headers: {
                'Content-Type': 'application/json'
            }});
        if (res.status === 200) {
            window.location.reload();
        }

    }



    const handleTagAdd = (req) => {
        if (req){
            if (currentReq.trim() !== '') {
                setReqs([...reqs, currentReq.trim()]);
                setCurrentReq('');
            }
        } else{
            if (currentBan.trim() !== '') {
                setBans([...bans, currentBan.trim()]);
                setCurrentBan('');
            }
        }
    };

    const handleTagRemove = (req, index) => {
        if (req){
            const updatedTags = reqs.filter((_, i) => i !== index);
            setReqs(updatedTags);
        } else{
            console.log("toggled")
            const updatedTags = bans.filter((_, i) => i !== index);
            setBans(updatedTags);
        }
    };

    return (
        <div>
        {!requirements &&(<div
        className="rounded-lg flex shadow-lg justify-around mt-8 p-1.5 bg-karl dark:bg-karl-dark bg-opacity-50 flex-wrap md:flex-nowrap">
        <div className="flex flex-col  p-5">
            <label htmlFor="name">Exercise Name:</label>
            <input className="input" type="text" id="name" onChange={(e) => setExerciseName(e.target.value)} required/>
            <label htmlFor="name">Exercise slug:</label>
            <input className="input" type="text" id="slug" onChange={(e) => setExerciseSlug(e.target.value)} required/>
            <div className="flex justify-between">
                <div className="flex flex-col">
                    <label htmlFor="startDate">Start Date:</label>
                    <input className="input text-lg" id="startDate" type="datetime-local" value={startDate}
                           onChange={handleStartDateChange}/>
                </div>
                <div className="flex flex-col">
                    <label htmlFor="endDate">End Date:</label>
                    <input className="input text-lg" id="endDate" type="datetime-local"
                           value={endDate}
                           onChange={handleEndDateChange}/>
                </div>
            </div>
        </div>
        <div className="flex flex-col w-full md:w-1/3 p-5 mt-8 justify-between">
            <FileUploader disabled={(zipFile !== null)} types={["zip"]} handleChange={(file) => {
                setTimeout(() => {
                    setZipFile(file);
                }, "500");
            }} children={
                <div className="bg-white p-1 rounded-md ">
                    <div
                        className="border-2 rounded-md w-full border-gray-400 border-dashed bg-white text-gray-700 flex flex-col text-center">
                        {zipFile === null ?
                            <div>
                                <img className="mt-1 w-8 mr-auto ml-auto"
                                     src="/images/icons/zip-file.svg"/>
                                <p>Select a ZIP file to upload</p>
                                <p className="text-xs mb-1">or drag and drop here</p>
                            </div> :
                            <div>
                                <img className="mt-1 w-8 mr-auto ml-auto"
                                     src="/images/icons/zip-file-minus.svg"/>
                                <p>{zipFile.name}</p>
                                <a onClick={() => {
                                    setZipFile(null);
                                }} className="underline" href="#">Remove file</a>
                            </div>
                        }
                    </div>
                </div>
            }/>

            <button onClick={()=>{uploadZip().then(r => console.log(r))}} className="rounded bg-blue-600 text-white p-3">
                Upload exercise
            </button>
        </div>
    </div>)}

    {requirements &&(
        <div className="rounded-lg shadow-lg mt-8 p-1.5 bg-karl dark:bg-karl-dark bg-opacity-50 ">

            <div className="tag-container flex flex-col ">
                <p>Required:</p>
                <div className="flex flex-wrap">

                    {reqs.map((tag, index) => (
                    <span key={index} className="tag bg-gray-300 rounded px-2 py-1 mr-2 mb-2 flex items-center">
                                        {tag}
                        <span className="cross text-red-500 ml-2 cursor-pointer" onClick={() => handleTagRemove(true,index)}>x</span>
                                    </span>
                ))}
                </div>
                <input
                    type="text"
                    placeholder="Type and press Enter to add tags"
                    value={currentReq}
                    onChange={(e) => setCurrentReq(e.target.value)}
                    onKeyDown={(e) => e.key === 'Enter' && handleTagAdd(true)}
                    className="input"
                />
            </div>

            <div className="tag-container flex flex-col">
                <p>Banned:</p>
                <div className="flex flex-wrap">
                {bans.map((tag, index) => (
                    <span key={index} className="tag bg-gray-300 rounded px-2 py-1 mr-2 mb-2 flex items-center">
                                        {tag}
                        <span className="cross text-red-500 ml-2 cursor-pointer" onClick={() => handleTagRemove(false, index)}>x</span>
                                    </span>
                ))}
                </div>
                <input
                    type="text"
                    placeholder="Type and press Enter to add tags"
                    value={currentBan}
                    onChange={(e) => setCurrentBan(e.target.value)}
                    onKeyDown={(e) => e.key === 'Enter' && handleTagAdd(false)}
                    className="input"
                />
            </div>
            <button onClick={createExercise} className="rounded bg-blue-600 text-white p-3">
                Create exercise
            </button>

        </div>)}
        </div>

    );
}


