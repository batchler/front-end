import React, { useState} from "react";
import {FileUploader} from "react-drag-drop-files";
import {ValidationTab} from "@/components/validation/validationTab";


export function ValidateTab(){
    const [zipFiles, setZipFiles] = useState(null);
    const [arrayZips, setArrayZips] = useState([]);

    const uploadZip = () =>{
        setArrayZips(prevArrayZips => [...prevArrayZips, ...Array.from(zipFiles)]);
        setZipFiles(null);
    }
    return (
        <div>
        <div
                className="rounded-lg flex justify-center shadow-lg mt-8 p-1.5 bg-karl dark:bg-karl-dark bg-opacity-50 flex-wrap md:flex-nowrap">
                <div className="flex flex-col w-full md:w-1/3 p-5 mt-5 justify-between">
                    <FileUploader types={["zip"]} multiple={true} handleChange={(files) => {
                        setTimeout(() => {
                            console.log(files)
                            setZipFiles(files);

                        }, "500");
                    }} children={
                        <div className="bg-white p-1 rounded-md ">
                            <div
                                className="border-2 rounded-md w-full border-gray-400 border-dashed bg-white text-gray-700 flex flex-col text-center">
                                {zipFiles === null ?
                                    <div>
                                        <img className="mt-1 w-8 mr-auto ml-auto"
                                             src="/images/icons/zip-file.svg"/>
                                        <p>Select a ZIP file to upload</p>
                                        <p className="text-xs mb-1">or drag and drop here</p>
                                    </div> :
                                    <div>
                                        <img className="mt-1 w-8 mr-auto ml-auto"
                                             src="/images/icons/zip-file-minus.svg"/>
                                        <p>Total files: {zipFiles.length}</p>
                                        <a onClick={() => {

                                            setZipFiles(null);
                                        }} className="underline text-red-500" href="#">Remove files</a>
                                    </div>
                                }
                            </div>
                        </div>
                    }/>

                    <button onClick={uploadZip} className="rounded bg-blue-600  text-white p-3 mt-4">
                        Upload Files And Validate
                    </button>
                </div>
            </div>

            {arrayZips &&  arrayZips.map((file,index) => (
                <ValidationTab zipFile={file}/>
            ))}


        </div>

    );
}


