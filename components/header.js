import React, {useEffect, useState} from "react";
import {useTheme} from "next-themes";


export function Header(){
    const {theme, setTheme} = useTheme();

    const toggleDarkMode = () => {
        setTheme(theme === 'light' ? 'dark' : 'light');
    };
    const [shouldRenderButton, setShouldRenderButton] = useState(false);

    useEffect(() => {
        setShouldRenderButton(true);
    }, []);

    return (
        <header className="font-edubeginner border-b pt-4 pb-4 text-2xl flex justify-between">
            <h1><span className="font-semibold text-3xl">S</span>cory <span
                className="font-semibold text-3xl">F</span>eedback</h1>
            {(shouldRenderButton && theme === 'dark') && (
                <button
                    className="bg-white text-primary-dark p-1 w-10 h-10 flex justify-center items-center rounded focus:outline-none "
                    onClick={toggleDarkMode}>
                        <span
                            className="material-icons-outlined hover:rotate-180 transition-all duration-700">light_mode</span>
                </button>)}
            {(shouldRenderButton && theme === 'light') && (
                <button
                    className="p-1 w-10 h-10 flex justify-center items-center rounded focus:outline-none bg-primary-dark text-white"
                    onClick={toggleDarkMode}>
                        <span
                            className="material-icons-outlined hover:rotate-180 transition-all duration-700">dark_mode</span>
                </button>)}

        </header>
    );
}


