/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  darkMode: ["class"],
  theme: {
    extend: {
      maxWidth: {
        '1/2': '50%',
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      fontFamily: {
        'edubeginner': ['"Edu SA Beginner"', 'cursive'],
        'arial':["Arial Rounded MT Std"]
      },
      colors: {
        "primary-dark": "rgba(55, 65, 81, 1)",
        "secondary-dark": "rgba(55, 65, 81, 0.7)",
        "dark": "rgba(85, 86, 86)",
        "primary-light": "rgba(243, 244, 246,0.3)",
        "karl": "rgb(237 236 237)",
        "karl-dark": "rgb(154, 153, 154)"
      }
    },
  },
  plugins: [],
}
