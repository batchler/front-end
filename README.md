<p align="center"><a href="https://batchler.depaepejonathan.be/" target="_blank"><img src="https://i.imgur.com/STJi0e0.png" width="400"></a></p>

# Batchler - Scory Feedback - Web App POC

[![Generic badge](https://img.shields.io/badge/Version-Alpha-red.svg)](https://shields.io/)


## Useful links

- Web app: https://batchler.depaepejonathan.be/
- API : https://api.depaepejonathan.be/

## How to run the Web app localy

### Required
-  Connection to the [API](https://api.depaepejonathan.be/)
-  Latest version of [NodeJs](https://nodejs.org/en/download/)


### Installing

- Clone the server into your preferred folder
    - `git clone gitLink`
- Install the packages
    - `npm install`
- Run the Web app
    -  `next build`
    -  `next start`

##
### Usefull packages to know
- Next-themes
    - Is used for the dark/light mode
- @headlessui/react
    - Useful ui components that works with react/next
- react-drag-drop-files
  - Library that makes it possible to have a drag and drop field

  

## Known bugs
- Currently no bugs found

## Credentials
Depaepe Jonathan
