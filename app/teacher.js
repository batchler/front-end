"use client";

import Image from 'next/image';
import {Tab} from "@headlessui/react";
import {Fragment, useEffect, useState} from 'react'
import {useTheme} from "next-themes";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPen, faGlobe, faCircleCheck} from "@fortawesome/free-solid-svg-icons";
import {CreateTab} from "@/components/lecturer/createTab";
import {ValidateTab} from "@/components/lecturer/validateTab";
import {PublishedTab} from "@/components/lecturer/publishedTab";


export default function Home() {
    const {theme, setTheme} = useTheme();

    const toggleDarkMode = () => {
        setTheme(theme === 'light' ? 'dark' : 'light');
    };
    const [shouldRenderButton, setShouldRenderButton] = useState(false);

    useEffect(() => {
        setShouldRenderButton(true);
    }, []);

    return (
        <div className="container 2xl ml-auto mr-auto">

            <header className="font-edubeginner border-b pt-4 pb-4 text-2xl flex justify-between">
                <h1><span className="font-semibold text-3xl">S</span>cory <span
                    className="font-semibold text-3xl">F</span>eedback</h1>
                {(shouldRenderButton && theme === 'dark') && (
                    <button
                        className="bg-white text-primary-dark p-1 w-10 h-10 flex justify-center items-center rounded focus:outline-none "
                        onClick={toggleDarkMode}>
                        <span
                            className="material-icons-outlined hover:rotate-180 transition-all duration-700">light_mode</span>
                    </button>)}
                {(shouldRenderButton && theme === 'light') && (
                    <button
                        className="p-1 w-10 h-10 flex justify-center items-center rounded focus:outline-none bg-primary-dark text-white"
                        onClick={toggleDarkMode}>
                        <span
                            className="material-icons-outlined hover:rotate-180 transition-all duration-700">dark_mode</span>
                    </button>)}

            </header>

            <Tab.Group>
                <Tab.List className="flex justify-between shadow-lg mt-4 p-1.5 rounded-lg bg-karl dark:bg-karl-dark">
                    <Tab  as={Fragment}>
                        {({selected}) => (
                            <button className={selected ? "tab-content-selected" : "tab-content"}>
                                <FontAwesomeIcon className="mr-2" icon={faPen}/>Create
                            </button>
                        )}</Tab>
                    <Tab as={Fragment}>
                        {({selected}) => (
                            <button className={selected ? "tab-content-selected" : "tab-content"}>
                                <FontAwesomeIcon className="mr-2" icon={faGlobe}/>Published
                            </button>
                        )}</Tab>
                    <Tab selected={true} as={Fragment}>
                        {({selected}) => (
                            <button className={selected ? "tab-content-selected" : "tab-content"}>
                                <FontAwesomeIcon className="mr-2" icon={faCircleCheck}/>
                                Validate
                            </button>
                        )}</Tab>
                </Tab.List>
                <Tab.Panels>
                    <Tab.Panel>
                        <CreateTab/>
                    </Tab.Panel>
                    <Tab.Panel>
                        <PublishedTab/>
                    </Tab.Panel>
                    <Tab.Panel>
                        <ValidateTab/>
                    </Tab.Panel>
                </Tab.Panels>
            </Tab.Group>
            <div>
            </div>
        </div>
    )
}
