"use client";


import React, {useState, Fragment} from 'react'
import {Header} from "@/components/header";
import {FileUploader} from "react-drag-drop-files";
import {Combobox, Transition} from "@headlessui/react";
import { ChevronUpDownIcon} from '@heroicons/react/20/solid'
import {ValidationTab} from "@/components/validation/validationTab";


const exercises = [
    {slug: "ex1", name: 'Exercise 1 (ex1)'},
    {slug: "ex2", name: 'Exercise 2 (ex2)'},
    {slug: "ex3", name: 'Exercise 3 (ex3)'},
    {slug: "ex4", name: 'Exercise 4 (ex4)'},
    {slug: "ex4.1", name: 'Exercise 4.1 (ex4.1)'},

]
const students = [
    {id: 1, name: 'Jonathan Depaepe'},
    {id: 2, name: 'Student 1'},
    {id: 3, name: 'Student 2'},
    {id: 4, name: 'Student 3'},
    {id: 5, name: 'Student 5'},
    {id: 6, name: 'Student 4'},
]


export default function Home() {
    const [zipFile, setZipFile] = useState(null);
    const [getExercises, setExercises] = useState(exercises[0])
    const [getStudents, setStudents] = useState(students[0])
    const [uploadedZip, setUploadedZip] = useState(null)
    const [stutQuery, setStutQuery] = useState('')
    const [exQuery, setExQuery] = useState('')
    const filteredStudents =
        stutQuery === ''
            ? students
            : students.filter((person) => {
                return person.toLowerCase().includes(stutQuery.toLowerCase())
            })
    const filteredExercises =
        exQuery === ''
            ? exercises
            : exercises.filter((person) => {
                return person.toLowerCase().includes(exQuery.toLowerCase())
            })

    const uploadZip = async () => {
        setUploadedZip(zipFile);
    }
    return (
        <div>
            <Header/>
            <div>
                <div
                    className="rounded-lg flex shadow-lg justify-around flex-col mt-8 p-1.5 bg-karl dark:bg-karl-dark bg-opacity-50 ">
                    <div className="flex justify-around flex-wrap md:flex-nowrap">
                        <div className="flex flex-col w-full md:w-1/3 p-5  justify-between">
                            <FileUploader disabled={(zipFile !== null)} types={["zip"]} handleChange={(file) => {
                                setTimeout(() => {
                                    setZipFile(file);
                                }, "500");
                            }} children={
                                <div className="bg-white p-1 rounded-md ">
                                    <div
                                        className="border-2 rounded-md w-full border-gray-400 border-dashed bg-white text-gray-700 flex flex-col text-center">
                                        {zipFile === null ?
                                            <div>
                                                <img className="mt-1 w-8 mr-auto ml-auto"
                                                     src="/images/icons/zip-file.svg"/>
                                                <p>Select a ZIP file to upload</p>
                                                <p className="text-xs mb-1">or drag and drop here</p>
                                            </div> :
                                            <div>
                                                <img className="mt-1 w-8 mr-auto ml-auto"
                                                     src="/images/icons/zip-file-minus.svg"/>
                                                <p>{zipFile.name}</p>
                                                <a onClick={() => {
                                                    setZipFile(null);
                                                }} className="underline" href="#">Remove file</a>
                                            </div>
                                        }
                                    </div>
                                </div>
                            }/>

                        </div>
                        <div className="flex p-5">
                            <div className="flex flex-col mr-2">
                                <p>Exercise: </p>
                                <Combobox value={getExercises} onChange={setExercises}>
                                    <div className="relative mt-1">
                                        <div
                                            className="relative w-full cursor-default overflow-hidden rounded-lg bg-white text-left shadow-md focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-blue-300 sm:text-sm">
                                            <Combobox.Input
                                                className="w-full border-none py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 focus:ring-0 dark:bg-white"
                                                displayValue={(person) => person.name}
                                                onChange={(event) => setExQuery(event.target.value)}
                                            />
                                            <Combobox.Button
                                                className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                <ChevronUpDownIcon
                                                    className="h-5 w-5 text-gray-400"
                                                    aria-hidden="true"
                                                />
                                            </Combobox.Button>
                                        </div>
                                        <Transition
                                            as={Fragment}
                                            leave="transition ease-in duration-100"
                                            leaveFrom="opacity-100"
                                            leaveTo="opacity-0"
                                            afterLeave={() => setExQuery('')}
                                        >
                                            <Combobox.Options
                                                className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                {filteredExercises.length === 0 && exQuery !== '' ? (
                                                    <div
                                                        className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                        Nothing found.
                                                    </div>
                                                ) : (
                                                    filteredExercises.map((person) => (
                                                        <Combobox.Option
                                                            key={person.id}
                                                            className={({active}) =>
                                                                `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                    active ? 'bg-blue-600 text-white' : 'text-gray-900'
                                                                }`
                                                            }
                                                            value={person}
                                                        >
                                                            {() => (
                                                                <>
                                                                    <span className="block truncate font-normal">
                                                                        {person.name}
                                                                    </span>
                                                                </>
                                                            )}
                                                        </Combobox.Option>
                                                    ))
                                                )}
                                            </Combobox.Options>
                                        </Transition>
                                    </div>
                                </Combobox>
                            </div>
                            <div className="flex flex-col">
                                <p>Student Name: </p>
                                <Combobox value={getStudents} onChange={setStudents}>
                                    <div className="relative mt-1">
                                        <div
                                            className="relative w-full cursor-default overflow-hidden rounded-lg bg-white text-left shadow-md focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-blue-300 sm:text-sm">
                                            <Combobox.Input
                                                className="w-full  border-none py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 focus:ring-0 dark:bg-white"
                                                displayValue={(person) => person.name}
                                                onChange={(event) => setStutQuery(event.target.value)}
                                            />
                                            <Combobox.Button
                                                className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                <ChevronUpDownIcon
                                                    className="h-5 w-5 text-gray-400"
                                                    aria-hidden="true"
                                                />
                                            </Combobox.Button>
                                        </div>
                                        <Transition
                                            as={Fragment}
                                            leave="transition ease-in duration-100"
                                            leaveFrom="opacity-100"
                                            leaveTo="opacity-0"
                                            afterLeave={() => setStutQuery('')}
                                        >
                                            <Combobox.Options
                                                className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                {filteredStudents.length === 0 && exQuery !== '' ? (
                                                    <div
                                                        className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                        Nothing found.
                                                    </div>
                                                ) : (
                                                    filteredStudents.map((person) => (
                                                        <Combobox.Option
                                                            key={person.id}
                                                            className={({active}) =>
                                                                `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                    active ? 'bg-blue-600 text-white' : 'text-gray-900'
                                                                }`
                                                            }
                                                            value={person}
                                                        >
                                                            {() => (
                                                                <span className='block truncate  font-normal'>
                                                                    {person.name}
                                                                </span>
                                                            )}
                                                        </Combobox.Option>
                                                    ))
                                                )}
                                            </Combobox.Options>
                                        </Transition>
                                    </div>
                                </Combobox>
                            </div>
                        </div>
                    </div>
                    <button disabled={uploadedZip!==null} onClick={uploadZip} className="rounded bg-blue-600 disabled:bg-blue-200 text-white p-3">
                        Validate Exercise
                    </button>
                </div>
                {uploadedZip &&(
                    <ValidationTab zipFile={uploadedZip}/>

                )}

            </div>
        </div>
    )
}
