
"use client"
import './globals.css'
import Providers from "./providers";
import 'material-icons/iconfont/material-icons.css'


export default function RootLayout({ children }) {

  return (
    <html lang="en">
      <body className="container 2xl ml-auto mr-auto">
      <Providers>
        {children}
      </Providers>
      </body>
    </html>
  )
}
