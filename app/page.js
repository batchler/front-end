"use client";

import Image from 'next/image';
import Link from "next/link";
import StudentPicture from "../public/images/undraw_Exams_re_4ios.png"
import TeacherPicture from "../public/images/undraw_Teacher_re_sico.png"
import {Header} from "@/components/header";

export default function Home() {

    return (
        <div>
            <Header/>
        <div className=" ml-auto mr-auto flex text-primary-dark justify-center font-bold mt-4 font-arial text-xl ">
            <Link className="flex flex-col bg-white shadow-2xl mr-5 hover:p-2 transition-all rounded-lg justify-between text-center p-5 w-80 h-80" href="student">
                <Image className="rounded-lg" src={StudentPicture}  alt={"Student picture"}/>
                <p className="">Student</p>
            </Link>
            <Link className="flex flex-col bg-white shadow-2xl mr-5 hover:p-2 transition-all justify-between text-center rounded-lg p-5 w-80 h-80"  href="teacher">
                <Image className="rounded-lg" src={TeacherPicture} alt={"Teacher picture"}/>
                <p>Teacher</p>
            </Link>
           </div>
        </div>
    )
}
