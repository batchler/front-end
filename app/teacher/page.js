"use client";

import {Tab} from "@headlessui/react";
import {Fragment, useEffect, useState} from 'react'
import {useTheme} from "next-themes";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPen, faGlobe, faCircleCheck} from "@fortawesome/free-solid-svg-icons";
import {CreateTab} from "@/components/lecturer/createTab";
import {ValidateTab} from "@/components/lecturer/validateTab";
import {PublishedTab} from "@/components/lecturer/publishedTab";
import {Header} from "@/components/header";


export default function Home() {


    return (
        <div >
            <Header/>
            <Tab.Group>
                <Tab.List className="flex justify-between shadow-lg mt-4 p-1.5 rounded-lg bg-karl dark:bg-karl-dark">
                    <Tab  as={Fragment}>
                        {({selected}) => (
                            <button className={selected ? "tab-content-selected" : "tab-content"}>
                                <FontAwesomeIcon className="mr-2" icon={faPen}/>Create
                            </button>
                        )}</Tab>
                    <Tab as={Fragment}>
                        {({selected}) => (
                            <button className={selected ? "tab-content-selected" : "tab-content"}>
                                <FontAwesomeIcon className="mr-2" icon={faGlobe}/>Published
                            </button>
                        )}</Tab>
                    <Tab selected={true} as={Fragment}>
                        {({selected}) => (
                            <button className={selected ? "tab-content-selected" : "tab-content"}>
                                <FontAwesomeIcon className="mr-2" icon={faCircleCheck}/>
                                Validate
                            </button>
                        )}</Tab>
                </Tab.List>
                <Tab.Panels>
                    <Tab.Panel>
                        <CreateTab/>
                    </Tab.Panel>
                    <Tab.Panel>
                        <PublishedTab/>
                    </Tab.Panel>
                    <Tab.Panel>
                        <ValidateTab/>
                    </Tab.Panel>
                </Tab.Panels>
            </Tab.Group>
            <div>
            </div>
        </div>
    )
}
